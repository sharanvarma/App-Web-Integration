import 'package:flutter/material.dart';
import 'constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Feed extends StatefulWidget {
  @override
  _FeedState createState() => _FeedState();
}

class _FeedState extends State<Feed> {
  final _firestore = Firestore.instance;

  @override
  void initState() {
    getfeed();
    super.initState();
  }

  void getfeed() async {
    await for (var snapshot in _firestore.collection('post').snapshots()) {
      for (var post in snapshot.documents) {
        print(post.data);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/');
                    },
                    child: Icon(
                      Icons.keyboard_backspace,
                      size: 30,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 60,
              ),
              StreamBuilder<QuerySnapshot>(
                stream: _firestore.collection('data').snapshots(),
                // ignore: missing_return
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    final datas = snapshot.data.documents;
                    List<Container> postWidgets = [];
                    for (var dataseg in datas) {
                      final DataNumber = dataseg.data['number'];
                      final DataNumber1 = dataseg.data['number1'];
                      final DataNumber2 = dataseg.data['number2'];

                      final DataWidget = Container(
                        width: double.infinity,
                        height: 100,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: bgblue,
                          boxShadow: [
                            new BoxShadow(
                              color: Colors.black26,
                              offset: new Offset(0, 10.0),
                              blurRadius: 10,
                            )
                          ],
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    '$DataNumber',
                                    style: logo.copyWith(fontSize: 22),
                                  ),
                                  Text(
                                    '$DataNumber1',
                                    style: logo.copyWith(fontSize: 22),
                                  ),
                                  Text(
                                    '$DataNumber2',
                                    style: logo.copyWith(fontSize: 22),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                      postWidgets.add(DataWidget);
                    }
                    return Expanded(
                      child: ListView(
                        children: postWidgets,
                      ),
                    );
                  }
                },
              ),
              SizedBox(
                height: 10,
              )
            ],
          ),
        ),
      ),
    );
  }
}
