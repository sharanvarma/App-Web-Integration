import 'package:flutter/material.dart';

var texto = InputDecoration(
    fillColor: Colors.white,
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(20),
    ));

const Color bgblue = Color(0xff354ad9);
const TextStyle logo = TextStyle(
  fontSize: 90,
  fontWeight: FontWeight.bold,
  color: Colors.white,
);
const TextStyle caption = TextStyle(
    fontSize: 15,
    color: Colors.grey,
    fontFamily: 'assets/fonts/Montserrat-SemiBold');
