const express = require("express");
const bodyparser = require("body-parser");
const logger = require("morgan");
const app = express();
const accountSid = 'ACd6e9dacb9ffb40b3db823a5adea4b75e';
const authToken = 'fa9fdc13a4b112f925ccc715c9c689e8';
const client = require('twilio')(accountSid, authToken);
const admin = require('firebase-admin');

const serviceAccount = {
      "type": "service_account",
      "project_id": "yestack-f9caf",
      "private_key_id": "6a226b9c408a5305df36ce847749240ef7c00c23",
      "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCgWO2k+ibGGpQt\nWQPu1r17Q62N1U+esEBq9rzE+gAI2pqkppqJxCXzyEJ4P4cZlcHk0He6VQhphRgK\nN5ELlylOSccG3pKB23NN5fNRlfIXToKoSYR4Oprl+gMnh+knNIlENcnHl/94gcoh\nM0JyCECpBmCHbVtBo9dkH68mEN0LOI6x790HRqy+EBadidUB/2DKsS5EiCQ2H4Lh\n1dTQxAV/N6ZpbMvltnzXBmXJ9mdmwlIrALeRjUK2r9giBDkOoprQpE8ovXI+cytt\npzvwQ+maGbtcIv7kwyfI6psC1S9L4vE58X94E/Kl0Gtj0nLvOF22ip9Yz+O5WlpD\nsYtC63BjAgMBAAECggEAHhA+UjnCjk8KbCwt5BhoA1XvktGlMXQLR+LGBsp2ujI+\nX1OLUvwvP3NTHxeOHXpqFZwObqWlw578IGJFtB4d1PtZMQ0G/Ajop/jOs4PBFaSX\nM7VA/IfEZg0HjWBakwqzF9Jb6Cuz7uSqwKnLMfUhJm+3Jn0W6IovBqs4HBN6VjFy\n2Y/Ad3ZgEDyhCHvQYFiWthtDDFkw02EYnXzLmLYc0kYdBUXZWvlpBzAQXj6i9JBe\ngWSQcYijHr4PgB5rw5mijXMORS5z5SDuc6JzdhSHvxHuCQAHXmYvqLa/cr/uXTc+\niIVhUBcwulEycIucBI28yXWYdiHq9onNhpPRuFt7XQKBgQDWtZ9ymG8B6eU6alQG\nG7ytq3KQxSk01CKmIB+P1xYCRjelnT39eK3/jmdJm5A0ICflICWSeT9bzFO1zLGQ\nqOD93meHUngQY9LUcPnlzo+xmqMTgUJNiXW/YnNBCJ4j1G8wgbkX24TyTVtTjPg2\naUrHfa/Q0yQo0JS7MtygG9f23QKBgQC/LwBmyFQb/L+W+eNcHuDPISlxyP5tcu10\noC5lx1G0wMnD/tKyHa4+/3Mwwk0MnSGYvz9RrZRcKY5+5VSTtW2l7Iwk007UNgzi\nWxwsG8QzwY0SMbFBj9r+5eaCRVB4dSMmjTMVxodr5t76PUBf6sKb9TK6hum4YPrJ\n8erBywlwPwKBgQC0TaMPfbjCt6M68e2Lj6Fbij1UG22mInrSXOmERlACKusyFaDo\nCX68u9467vsi+tnlzKAgp9cwBJ9LbRCz9U2zgyAXgIa45Ms4gYMpm9QJhl1p1MNU\nD5ok1M0KkO0UE6In7tf7k+nJ/HIz8I7ESpl0ezS4vYaBC9/KcETb42XxtQKBgDx8\n43ShhzL/qaJb15pqzpfaFyHABPed+BEBKQzcnjZBFNUlB4aBK4MVEV3o67XE9T89\nqwvtjlYGc+YkKrk9Gr73c3OnQ3aSz0rCpswx0MbLQxXxkPJ8UD57gI3Q3l/uN7vX\nysTl9iymc//aZ0axoPb1AiLP0pyaWl6QYsUDty3DAoGAVMDEIyOhlLyWVriKcSAS\n7Cc/4V9uHYo6GrCQHLTLgnURMl1Na3b89b10xqnqzhG0EGH1H5ePPQvbwAsDoI/h\n22GBIkJeHN1xuE1eddKq0Qu6tbKIcomhjmkXcEQ/lkKJIsg5N2I2XgnCQWrFYPmM\nzuOBOcBbObfskR4FWg3prwQ=\n-----END PRIVATE KEY-----\n",
      "client_email": "firebase-adminsdk-sqq6f@yestack-f9caf.iam.gserviceaccount.com",
      "client_id": "116526160634589111340",
      "auth_uri": "https://accounts.google.com/o/oauth2/auth",
      "token_uri": "https://oauth2.googleapis.com/token",
      "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
      "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-sqq6f%40yestack-f9caf.iam.gserviceaccount.com"
};

admin.initializeApp({
      credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();

app.use(bodyparser.urlencoded({extended: true}));

app.set("view engine","ejs");

app.use(logger('dev'));

app.use(express.static('views'));
app.set('views', __dirname + '/views');

app.get("/", (req,res) => {
	res.render("home");
});

app.get("/getData", (req,res) => {
      var webNum = null;
      var appNum = null;
      var phoneNumber = "";

      //Retrieving Data from firebase firestore.      
      db.collection('data').doc('alldata').get()
      .then(doc => {
            if(!doc.exists){
                  console.log("No such document!");
            }
            phoneNumber = doc._fieldsProto.number.stringValue;
            appNum = doc._fieldsProto.number1.integerValue;
            webNum = parseInt(doc._fieldsProto.number2.stringValue, 10);
            var sum = parseInt(appNum, 10) + parseInt(webNum, 10);
            var diff = appNum - webNum;
            res.render("results", {phoneNumber:phoneNumber,webNum:webNum,appNum:appNum,sum:sum,diff:diff});
            
            //Whatsapp Message Code
            client.messages 
            .create({ 
            body: 'Message from #Define Team: Your inputs have been processed. The sum is ' + sum + ' and the difference is ' + diff, 
            from: 'whatsapp:+14155238886',       
            to: 'whatsapp:+91' + phoneNumber 
            }) 
            .then(message => console.log("Message Sent. Message SID:",message.sid)) 
            .done();

            //SMS Message Code For Twilio
            client.messages
                  .create({
                  body: 'Message from #Define Team: Your inputs have been processed. The sum is ' + sum + ' and the difference is ' + diff, 
                  from: '+12055258265',
                  statusCallback: 'http://postb.in/1234abcd',
                  to: '+91' + phoneNumber
                  })
                  .then(message => console.log("SMS Sent!",message.sid));

app.listen(3000, () => {
	console.log("Server running on port 3000.");
});